﻿using System;

using Xamarin.Forms;

namespace Group6Application
{
	public class MenuPage : ContentPage
	{
		MasterDetailPage master;

		TableView tableView;

		public MenuPage ()
		{
			Title = "Group6Application";
			Icon = "slideout.png";

			var section = new TableSection () {
				new TextCell { Text = "Products" },
				new TextCell { Text = "Manufacturers" },
				new TextCell { Text = "Categories" }
			};

			var root = new TableRoot () {section};

			tableView = new TableView ()
			{
				Root = root,
				Intent = TableIntent.Menu,
			};

			var logoutButton = new Button { Text = "Logout" };
			logoutButton.Clicked += (sender, e) => {
				//App.Current.Logout();
			};

			Content = new StackLayout {
				BackgroundColor = Color.Gray,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					tableView,
					logoutButton
				}
			};
		}
	}
}


