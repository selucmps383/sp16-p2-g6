﻿using Xamarin.Forms;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Linq;


namespace Group6Application
{
	public partial class CategoriesView : ContentPage
	{
		private bool isInitialized;

		public User currentUser = new User();

		public CategoriesView (User user)
		{

			currentUser = user;
			InitializeComponent ();
			BindingContext = this;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();


			BackgroundImage = "androidWallpaper.jpg";

			if (isInitialized) {
				return;
			}

			ApiCalls apiCall = new ApiCalls ();

			ObservableCollection<Category> categories = await apiCall.GetCategories(currentUser);

			listView.ItemsSource = categories;

			isInitialized = true;

		}

	}
}

