﻿using System;
using Xamarin.Forms;

namespace Group6Application
{
	public class MainPage : MasterDetailPage
	{
		public MainPage ()
		{
			Master = new MenuPage ();
			Detail = new DetailPage ();
		}
	}
}

