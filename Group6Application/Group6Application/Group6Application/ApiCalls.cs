﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace Group6Application
{
	public class ApiCalls
	{
		private const string baseUrl = "http://147.174.168.86:50396/";
		public User currentUser = new User();

		public async Task<ObservableCollection<Product>> GetProducts()
		{
			var apiUrl = "api/products";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.GET);
			var productResponse = await restClient.Execute<ObservableCollection<Product>> (request);
			return productResponse.Data;
		}

		public async Task<NavigationPage> CreateProduct(Product product, User user)
		{
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest ("api/products", Method.POST);
			request.AddBody (product);
			request.AddHeader ("ApiKey", user.ApiKey);
			request.AddHeader ("UserId", user.Id);
			currentUser = user;

			try {
				var productResponse = await restClient.Execute<Product> (request);
				return new NavigationPage (new ProductsView (currentUser));
			} catch {
				return null;
			}
		}	

	public async Task<Product> EditProduct(Product product)
		{
			var apiUrl = "api/product/addOrUpdateProduct/";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.POST);
			request.AddBody (product);
			var productResponse = await restClient.Execute<Product> (request);

			//if (productResponse.StatusCode == Http.OK) {
			return productResponse.Data;
			//}
	}

		public async Task<Product> DeleteProduct(User user, Product product)
		{
			var apiUrl = "api/products";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.DELETE);
			request.AddParameter ("id", product.Id);
			request.AddHeader ("ApiKey", user.ApiKey);
			request.AddHeader ("UserId", user.Id);
		
			var productResponse = await restClient.Execute<Product> (request);

			return productResponse.Data;
		}

		public async Task<ObservableCollection<Manufacturer>> GetManufacturers(User user)
		{
			var apiUrl = "api/manufacturers";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.GET);
			request.AddHeader ("ApiKey", user.ApiKey);
			request.AddHeader ("UserId", user.Id);
			var manufacturerResponse = await restClient.Execute<ObservableCollection<Manufacturer>> (request);
			return manufacturerResponse.Data;
		}

		public async Task<ObservableCollection<Category>> GetCategories(User user)
		{
			var apiUrl = "api/categories";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.GET);
			request.AddHeader ("ApiKey", user.ApiKey);
			request.AddHeader ("UserId", user.Id);
			var categoryResponse = await restClient.Execute<ObservableCollection<Category>> (request);
			return categoryResponse.Data;
		}
		public async Task<User> GetApiKey(string email, string password)
		{
			var apiUrl = "api/apikey";
			var restClient = new RestClient (baseUrl);
			var request = new RestRequest (apiUrl, Method.GET);
			request.AddParameter ("email", email);
			request.AddParameter ("password", password);

			try {
				var keyResponse = await restClient.Execute<User>(request);

				return keyResponse.Data;
			} catch(Exception e) {
				return null;
			}
		}
			
	}
}

