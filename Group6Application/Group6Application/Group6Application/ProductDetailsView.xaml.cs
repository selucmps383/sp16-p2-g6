﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace Group6Application
{
	public partial class ProductDetailsView : ContentPage
	{
		Product product;
		ObservableCollection<Product> products;
		ApiCalls apiCalls = new ApiCalls();
		public User currentUser = new User();

		public ProductDetailsView (ObservableCollection<Product> products, Product product, User user)
		{
			this.product = product;
			this.products = products;
			currentUser = user;

			InitializeComponent ();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing ();
			BackgroundImage = "androidWallpaper.jpg";

			prodName.Text = "Item Name:" + product.Name;
			prodCreatedDate.Text = "Created Date: " + product.CreatedDate;
			prodLastModifiedDate.Text = "Last Modified Date: " + product.LastModifiedDate;
			prodPrice.Text = "Price: $" + product.Price;
			prodQuantity.Text = "Quantity:" + product.InventoryCount;
			prodCategory.Text = "Category:" + product.Category;
			prodManufacturer.Text = "Manufacturer: " + product.Manufacturer;


			btnEdit.Clicked += (sender, e) => 
			{
				var editProduct = new EditProduct(products, product, currentUser);
				Navigation.PushAsync(editProduct, true);
			};

		}

		public async void btnDeleteClicked(Object sender, EventArgs e)
		{
			
			//this.Navigation.PushModalAsync (confirmDelete);
			var deletedProduct = products.FirstOrDefault (p => p.Id == this.product.Id);
			if (deletedProduct != null) {
				await apiCalls.DeleteProduct (currentUser, deletedProduct);
				products.Remove (deletedProduct);
				//this.Navigation.PushModalAsync (deleteSuccess)
				//this.Navigation.PopModalAsync ();
				await this.Navigation.PopAsync ();
			}
		}
	}
}

