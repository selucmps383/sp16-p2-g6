﻿using System;
using System.Collections.Generic;

namespace Group6Application
{
	public class Manufacturer
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public List<Product> RelatedProducts { get; set; }
	}
}

