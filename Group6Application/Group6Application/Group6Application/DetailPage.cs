﻿using System;

using Xamarin.Forms;

namespace Group6Application
{
	public class DetailPage : ContentPage
	{
		public DetailPage ()
		{
			BackgroundColor = new Color (0, 0, 1, 0.2);

			var text = "Slide > to see the master / menu";

			text = @"Click the action bar dots to see the master / menu";

			Content = new StackLayout {
				HorizontalOptions = LayoutOptions.Center,
				Padding = new Thickness (10, 40, 10, 10),
				Children = {
					new Label { Text = text }
				}
			};
		}
	}
}


