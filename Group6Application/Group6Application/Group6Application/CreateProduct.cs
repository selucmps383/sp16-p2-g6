﻿using System;

using Xamarin.Forms;

namespace Group6Application
{
	public class CreateProduct : ContentPage
	{
		ApiCalls apiCall = new ApiCalls ();
		public User currentUser = new User();
		Product product = new Product();
		//var createProduct = apiCall.CreateProduct ();

		public CreateProduct (User user) 
		{
			currentUser = user;
			Title = "Create Product";
			BackgroundImage = "androidWallpaper.jpg";

			var prodName = new Entry () { Placeholder = "Name", TextColor = Color.White };
			var prodPrice = new Entry () { Placeholder = "Price", TextColor = Color.White };
			var prodInventory = new Entry () { Placeholder = "Inventory Count", TextColor = Color.White };
			var prodCategory = new Entry () { Placeholder = "Category", TextColor = Color.White };
			var prodManufacturer = new Entry () { Placeholder = "Manufacturer", TextColor = Color.White };
			var Create = new Button {Text = "Create Product", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") };


			Content = new StackLayout {
				Spacing = 20, Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					prodName,
					prodPrice,
					prodInventory,
					prodCategory,
					prodManufacturer,
					Create
				}
			};

			Create.Clicked += async (sender, e) =>
			{
				product.Name = prodName.Text;
				product.Price = Convert.ToDouble(prodPrice.Text);
				product.InventoryCount = Convert.ToInt32(prodInventory.Text);
				product.Category = prodCategory.Text;
				product.Manufacturer = prodManufacturer.Text;

				if (String.IsNullOrEmpty (prodName.Text) || String.IsNullOrEmpty (prodPrice.Text) || 
					String.IsNullOrEmpty (prodInventory.Text) || String.IsNullOrEmpty (prodCategory.Text) || 
					String.IsNullOrEmpty (prodManufacturer.Text)) {
					DisplayAlert ("Validation Error", "You cannot leave any fields blank", "Please try again");
				} else {
					NavigationPage nextPage = await apiCall.CreateProduct(product, currentUser);
					App.Current.MainPage = nextPage;
					//await Navigation.PopAsync ();
				}
			};

		}
			
	}
}


