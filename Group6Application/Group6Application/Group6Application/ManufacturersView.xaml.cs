﻿using Xamarin.Forms;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Linq;


namespace Group6Application
{
	public partial class ManufacturersView : ContentPage
	{
		private bool isInitialized;

		public User currentUser = new User();

		public ManufacturersView (User user)
		{
			currentUser = user;
			InitializeComponent ();
			BindingContext = this;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();


			BackgroundImage = "androidWallpaper.jpg";

			if (isInitialized) {
				return;
			}

			ApiCalls apiCall = new ApiCalls ();

			ObservableCollection<Manufacturer> manufacturers = await apiCall.GetManufacturers(currentUser);

			listView.ItemsSource = manufacturers;

			isInitialized = true;

		}

	}
}

