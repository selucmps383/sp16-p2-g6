﻿using System;

namespace Group6Application
{
	public interface ILoginManager
	{
		void ShowMainPage ();
		void Logout ();
	}
}

