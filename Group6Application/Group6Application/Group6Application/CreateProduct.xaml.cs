﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Group6Application
{
	public partial class CreateProduct : ContentPage
	{
		public CreateProduct ()
		{
			InitializeComponent ();
		}

		Title = "Create Product",
		Icon = "Profile.png",
		Content = new StackLayout {
			Spacing = 20, Padding = 50,
			VerticalOptions = LayoutOptions.Center,
			Children = {
				new Entry { Placeholder = "Name" },
				new Entry { Placeholder = "Price" },
				new Entry { Placeholder = "Inventory Count" },
				new Entry { Placeholder = "Category" },
				new Entry { Placeholder = "Manufacturer" },
				new Button {
					Text = "Create Product",
					TextColor = Color.White,
					BackgroundColor = Color.FromHex("77D065") }
			}
		}
	}
}

