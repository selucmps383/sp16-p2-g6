﻿using System;
using System.Collections.Generic;

namespace Group6Application
{

	public class Product
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime LastModifiedDate { get; set; }
		public double Price { get; set; }
		public int InventoryCount { get; set; }
		public string Category { get; set; }
		public string Manufacturer { get; set; }
		public List<Category> Categories { get; set; }
		public List<Manufacturer> Manufacturers { get; set; }
	}
}

