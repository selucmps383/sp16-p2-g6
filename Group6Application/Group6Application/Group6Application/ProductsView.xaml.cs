﻿using Xamarin.Forms;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Linq;


namespace Group6Application
{
	public partial class ProductsView : ContentPage
	{
		private bool isInitialized;

		public User currentUser = new User();

		public ProductsView (User user)
		{

			currentUser = user;
			InitializeComponent ();
			BindingContext = this;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();


			BackgroundImage = "androidWallpaper.jpg";

			if (isInitialized) {
				return;
			}
				
			ApiCalls apiCall = new ApiCalls ();

			ObservableCollection<Product> products = await apiCall.GetProducts();
	
			listView.ItemsSource = products;
				
			btnCreate.Clicked += (sender, e) => 
			{
				Navigation.PushAsync(new CreateProduct(currentUser));
			};
				
			listView.ItemTapped += (sender, e) => 
			{	
				var selectedProduct = (Product)e.Item;
				var productDetailsPage = new ProductDetailsView(products, selectedProduct, currentUser);
				Navigation.PushAsync(productDetailsPage, true);
			};

			isInitialized = true;

		}

	}
}
