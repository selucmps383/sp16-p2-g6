﻿using System;
using Xamarin.Forms;

namespace Group6Application
{
	public class MainPageCS : TabbedPage
	{
		public User currentUser = new User();

		public MainPageCS (User user)
		{
			currentUser = user;
			var navigationPage = new NavigationPage (new ManufacturersView (currentUser));
			navigationPage.Title = "Manufacturers";

			Children.Add (new NavigationPage(new ProductsView(currentUser)));
			Children.Add (navigationPage);

		}
	}
}

