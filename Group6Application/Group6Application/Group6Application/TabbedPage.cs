﻿using System;
using Xamarin.Forms;

namespace Group6Application
{
	public class TabbedPage2 : TabbedPage
	{
		public TabbedPage2 ()
		{
			this.Title = "TabbedPage";
			this.Children.Add (new ContentPage 
				{
					Title = "Blue",
					Content = new BoxView
					{
						Color = Color.Blue,
						HeightRequest = 100f,
						VerticalOptions = LayoutOptions.Center
					},
				}
			);
			this.Children.Add (new ContentPage {
				Title = "Blue and Red",
				Content = new StackLayout {
					Children = {
						new BoxView { Color = Color.Blue },
						new BoxView { Color = Color.Red}
					}
				}
			});
		}
	}
}

