﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace Group6Application
{
	public class EditProduct : ContentPage
	{
		Product product;
		ObservableCollection<Product> products;
		ApiCalls apiCalls = new ApiCalls();
		public User currentUser = new User();

		public EditProduct (ObservableCollection<Product> products, Product product, User user)
		{
			this.product = product;
			this.BindingContext = product;
			currentUser = user;

			var prodPrice = new Entry () { Text = product.Price.ToString() };
			prodPrice.SetBinding (Entry.TextProperty, "Price");
			var prodInventory = new Entry () { Text = product.InventoryCount.ToString() };
			prodInventory.SetBinding (Entry.TextProperty, "InventoryCount");


			Title = "Edit Product";
			BackgroundImage = "androidWallpaper.jpg";
			Content = new StackLayout {
				Spacing = 20, Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					new Entry { Placeholder = "Name", Text=product.Name },
					prodPrice,
					prodInventory,
					new Entry { Placeholder = "Category", Text=product.Category},
					new Entry { Placeholder = "Manufacturer", Text=product.Manufacturer },
					new Button {
						Text = "Update Product",
						TextColor = Color.White,
						BackgroundColor = Color.FromHex ("77D065")
					}
				}
			};
		}
	}
}


