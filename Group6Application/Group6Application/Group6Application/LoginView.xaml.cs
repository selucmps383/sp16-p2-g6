﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Group6Application
{
	public partial class LoginView : ContentPage
	{
		public User currentUser = new User();
		private bool isInitialized;

		Entry usernameEntry;
		Entry passwordEntry;
		Button button;

		public LoginView ()
		{
			OnAppearing ();
			InitializeComponent ();
			BindingContext = this;
		}


		protected void OnAppearing()
		{
			base.OnAppearing ();
		
			if (isInitialized) {
				return;
			}
			ApiCalls apiCall = new ApiCalls ();
			//var usernameEntry = new Entry {Placeholder = "Username"};
			//var passwordEntry = new Entry {Placeholder = "Password"};
			//var button = new Button { Text = "Login" };
			Title = "Login";
			BackgroundImage = "androidWallpaper.jpg";
			Content = new StackLayout {
				Spacing = 20, Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(usernameEntry = new Entry { Placeholder = "Email" }),
					(passwordEntry = new Entry { Placeholder = "Password", IsPassword = true }),
					(button = new Button { Text = "Login", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") })
				}
			};

			button.Clicked += async (sender, e) => {
				if (String.IsNullOrEmpty (usernameEntry.Text) || String.IsNullOrEmpty (passwordEntry.Text)) {
					DisplayAlert ("Validation Error", "Email and Password are required", "Please try again");
				} else {
					var apiKey = await apiCall.GetApiKey (usernameEntry.Text, passwordEntry.Text);

					if (apiKey != null) {
						currentUser = apiKey;
						App.Current.MainPage = new NavigationPage (new ProductsView (currentUser));
					} else {
						DisplayAlert ("Validation Error", "You entered the wrong email or password", "Please Try again");
					}
				}
				isInitialized = true;
			};
		}
	}
}

