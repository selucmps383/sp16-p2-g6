﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace _383_PhaseII_Group6_API.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<Product> RelatedProducts { get; set; }

    }
}