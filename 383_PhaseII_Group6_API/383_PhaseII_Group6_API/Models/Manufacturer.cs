﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _383_PhaseII_Group6_API.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public List<Product> RelatedProducts { get; set; }
    }
}