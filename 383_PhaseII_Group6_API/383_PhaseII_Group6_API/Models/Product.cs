﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _383_PhaseII_Group6_API.Models
{
    public class Product
    {

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Name")]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Last Modified Date")]
        public DateTime? LastModifiedDate { get; set; }

        [Display(Name = "Price")]
        public double Price { get; set; }
        [Required]
        [Display(Name = "Inventory Count")]
        public int InventoryCount { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Category")]
        public string Category { get; set; }

        [StringLength(150)]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        public virtual List<Sale> SalesWithProduct { get; set; }

        public List<Category> Categories { get; set; }

        public List<Manufacturer> Manufacturers { get; set; }
    }
}