﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _383_PhaseII_Group6_API.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Email")]
        //[Index(IsUnique = true)]
        public string Email { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [StringLength(150)]
        [Display(Name = "ApiKey")]
        public string ApiKey { get; set; }
        [Required]
        [StringLength(150, MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}