﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _383_PhaseII_Group6_API.Models
{
    public class Sale
    {
        public Sale()
        {
            var now = DateTime.Now.ToUniversalTime();
            Date = new DateTime(now.Year, now.Month, now.Day,
                                    now.Hour, now.Minute, now.Second);
            this.Products = new List<Product>();

        }
        [Key]
        public int Id { get; set; }
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Display(Name = "Total Amount")]
        public double TotalAmount { get; set; }

        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}