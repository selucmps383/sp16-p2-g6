﻿angular.module('InventoryApp', [])
    .controller('InventoryCtrl', function($scope, $http) {
        $scope.items = [];
        $scope.cart = [];
        $scope.quantity = 0;
        $scope.itemName = "";
        $scope.showModal = false;
        $scope.showModalQ = false;
        $scope.showModalB = false;
        $scope.hidden = false;
        $scope.visible = false;
        $scope.visibleI = false;

        $scope.toggleModal = function() {
            $scope.showModal = !$scope.showModal;
        };
        $scope.test = function (quantity) {
            $scope.quantity = quantity;
            var found = false;
            if (angular.isNumber($scope.quantity)) {
                $scope.showModalQ = !$scope.showModalQ;
                $http.get("api/product/addToCart/" + $scope.itemName + "/" + quantity).success(function (data, status, headers, config) {
                    console.log(data);
                    for (var i = 0; i < $scope.cart.length; i++) {
                        console.log("hit");
                        if (data.Name == $scope.cart[i].Name) {
                            found = true;
                            $scope.cart[i].InventoryCount = $scope.cart[i].InventoryCount + data.InventoryCount;
                            $scope.cart[i].Price = $scope.cart[i].Price * $scope.cart[i].InventoryCount;
                        }
                    }
                    if (found == false) {
                        data.Price = data.Price * data.InventoryCount;
                        $scope.cart.push(data);
                    }
                    console.log($scope.cart);

                });
                $scope.visible = false;
            } else {
                $scope.visible = true;
            }
        }

        $scope.listOfProducts = function() {
            $scope.items = [];
            $http.get("/api/products").success(function(data, status, headers, config) {
                console.log(data);
                $scope.items = data;
                console.log($scope.items);

            });
        }

        $scope.productsByCategory = function (category) {
            console.log("hit");
            $scope.items = [];
            $http.get("/api/product/bycategory/" + category).success(function (data, status, headers, config) {
                console.log(data);
                $scope.items = data;
                console.log($scope.items);

            });
        }
        $scope.purchaseItems = function () {
            $scope.showModal = false;
            $scope.showModalB = true;
            $scope.hidden = false;

        }
        $scope.addItemsToSale = function (email) {
            console.log(email);
            console.log($scope.cart);
            if (email.indexOf('@') > -1) {
                $scope.showModalB = !$scope.showModalB;
                $http({
                    url: '/home/PurchaseItems',
                    method: 'POST',
                    data: {
                        email: email,
                        listOfProducts: $scope.cart

                    }
                }).success(function (data, status, headers, config) {
                    $scope.cart = [];
                    $scope.hidden = true;
                    $http.get("/api/products").success(function (data, status, headers, config) {
                        console.log(data);
                        $scope.items = data;
                        console.log($scope.items);

                    });
                });
                $scope.visibleI = false;
            } else {
                $scope.visibleI = true;
            }
        }

        $scope.productsByManufacturer = function (manufacturer) {
            console.log("hit");
            $scope.items = [];
            $http.get("/api/product/bymanufacturer/" + manufacturer).success(function (data, status, headers, config) {
                console.log(data);
                $scope.items = data;
                console.log($scope.items);

            });
        }

        $scope.addToCart = function (itemName) {
            $scope.itemName = itemName;
            console.log(itemName);
            $scope.showModalQ = true;
        }
    })
    .directive('modal', function() {
        return {
            template: '<div class="modal fade">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
                '</div>' +
                '<div class="modal-body" ng-transclude></div>' +
                '</div>' +
                '</div>' +
                '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function(value) {
                    if (value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function() {
                    scope.$apply(function() {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function() {
                    scope.$apply(function() {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        }
    });
