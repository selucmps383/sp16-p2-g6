﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _383_PhaseII_Group6_API.Models;
using _383_PhaseII_Group6_API.authenication;

namespace _383_PhaseII_Group6_API.Controllers
{
    public class ProductsController : ApiController
    {
        private ProjectContext db = new ProjectContext();
        // GET: api/Products
        public IQueryable<Product> GetProducts()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Products;
        }
        [Route("api/product/addToCart/{name}/{amount}")]
        [HttpGet]
        public IHttpActionResult addToCart(string Name, int Amount)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var selectedItem = db.Products.FirstOrDefault(u => u.Name == Name);
            selectedItem.InventoryCount = Amount;
            return Ok(selectedItem);
        }
        [Route("api/product/bymanufacturer/{manufacturer}")]
        [HttpGet]
        public IHttpActionResult productsByManufacturer(string manufacturer)
        {
            List<Product> products = db.Products.Where(u => u.Manufacturer == manufacturer).ToList();
            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        [Route("api/product/bycategory/{catogory}")]
        [HttpGet]
        public IHttpActionResult productsByCategory(string catogory)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Product> products = db.Products.Where(u => u.Category == catogory).ToList();
            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

       // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return BadRequest();
            }

            db.Entry(product).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ApiKeyValidation]
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            var now = DateTime.Now.ToUniversalTime();
            var date = new DateTime(now.Year, now.Month, now.Day,
                                    now.Hour, now.Minute, now.Second);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(product);
            var category = db.Categories.FirstOrDefault(u => u.Name == product.Name);
            var manufacture = db.Manufacturers.FirstOrDefault(u => u.Name == product.Manufacturer);
            if (category == null)
            {
                db.Categories.Add(new Category {Name = product.Category});
            }
            if (manufacture == null)
            {
                db.Manufacturers.Add(new Manufacturer {Name = product.Manufacturer});
            }
            var CreatedDate = new DateTime(0001, 3, 21, 2, 0, 0);
            if (product.CreatedDate.Value.Year == CreatedDate.Year )
            {
                product.CreatedDate = date;
            }
            product.LastModifiedDate = date;
            db.Products.AddOrUpdate(product);

            db.SaveChanges();

            return Ok(product);
        }

        // DELETE: api/Products/5
        [ApiKeyValidation]
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.Id == id) > 0;
        }
    }
}