﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using _383_PhaseII_Group6_API.Models;
using System.Data.Entity.Migrations;
using System.Globalization;

namespace _383_PhaseII_Group6_API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public void PurchaseItems(string email, List<Product> listOfProducts)
        {
            using (ProjectContext db = new ProjectContext())
            {
                double amount = 0;

                MailMessage mail = new MailMessage("jessie.badon@selu.edu", email);
                using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
                {

                    client.Credentials = new System.Net.NetworkCredential("CMPSProject383@gmail.com", "selu2016383");
                    client.EnableSsl = true;
                    mail.From = new MailAddress("email@gmail.com");
                    mail.IsBodyHtml = true;
                    mail.Subject = "Purchase from CMPS store";

                    mail.Body += "<table width='100%' style='border:Solid 1px Black;'>";
                    mail.Body += "<tr>";
                    mail.Body += "<th style='text-align: left'>Name</th>" + "<th style='text-align: left'> Price </th>";
                    mail.Body += "<th style='text-align: left'> Inventory Quantity </th>";
                    mail.Body += "<th style='text-align: left'> Category </th>";
                    mail.Body += "<th style='text-align: left'> Manufacturer </th>";
                    mail.Body += "</tr>";
                    

                    foreach (var item in listOfProducts)
                    {
                        mail.Body += "<tr>";
                        amount += (int)item.Price;
                        Product curritem = db.Products.FirstOrDefault(u => u.Name == item.Name);
                        curritem.InventoryCount = curritem.InventoryCount - item.InventoryCount;
                        db.Products.AddOrUpdate(curritem);
                        db.SaveChanges();
                        mail.Body += "<td>" + item.Name + "</td>" + "<td>" + item.Price.ToString("C", CultureInfo.CurrentCulture) + "</td>" + "<td>" + item.InventoryCount + "</td>" + "<td>" + item.Category + "</td>" + "<td>" + item.Manufacturer + "</td>";
                        mail.Body += "</tr>";
                    }
                    foreach (var item in listOfProducts)
                    {

                        Product curritem = db.Products.FirstOrDefault(u => u.Name == item.Name);
                        curritem.SalesWithProduct = new List<Sale>();
                        Sale newSale = new Sale();
                        newSale.TotalAmount = amount;
                        newSale.EmailAddress = email;
                        curritem.SalesWithProduct.Add(newSale);
                        db.Products.AddOrUpdate(curritem);
                        db.SaveChanges();
                    }

                    
                    mail.Body += "</table>";
                    mail.Body += "<br />";
                    mail.Body += "<br />";
                    mail.Body += "<label>Total:" + " " + amount.ToString("C", CultureInfo.CurrentCulture);
                    client.Send(mail);
                }
            }
        }
    }
}
