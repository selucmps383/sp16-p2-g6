﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Security;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using _383_PhaseII_Group6_API.Models;

namespace _383_PhaseII_Group6_API.Controllers
{
    public class ApiKeyController : ApiController
    {
        private ProjectContext db = new ProjectContext();

     
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetApiKey(string email, string password)
        {
            if (IsValid(email,password))
            {
                using (var rng = new RNGCryptoServiceProvider())
                {
                    FormsAuthentication.SetAuthCookie(email, true);
                    var bytes = new byte[16];
                    rng.GetBytes(bytes);
                    var user = db.Users.FirstOrDefault(u => u.Email == email);
                    user.ApiKey = Convert.ToBase64String(bytes);
                    db.Users.AddOrUpdate(user);
                    db.SaveChanges();
                    return Ok(user);
                }
            }else if (!IsValid(email, password) && email != null && password != null)
            {
                var myCustomMessage = "Email or password is incorrect";

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, myCustomMessage));
            }
            return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest,"Email or Password is missing."));
        }
        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }
    }
}
