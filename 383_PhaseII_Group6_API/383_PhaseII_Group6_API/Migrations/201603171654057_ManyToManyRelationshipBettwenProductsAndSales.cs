namespace _383_PhaseII_Group6_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManyToManyRelationshipBettwenProductsAndSales : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Sale_Id", "dbo.Sales");
            DropIndex("dbo.Products", new[] { "Sale_Id" });
            CreateTable(
                "dbo.SaleProducts",
                c => new
                    {
                        Sale_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sale_Id, t.Product_Id })
                .ForeignKey("dbo.Sales", t => t.Sale_Id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: true)
                .Index(t => t.Sale_Id)
                .Index(t => t.Product_Id);
            
            AlterColumn("dbo.Products", "CreatedDate", c => c.DateTime());
            AlterColumn("dbo.Products", "LastModifiedDate", c => c.DateTime());
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String());
            DropColumn("dbo.Products", "Sale_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Sale_Id", c => c.Int());
            DropForeignKey("dbo.SaleProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.SaleProducts", "Sale_Id", "dbo.Sales");
            DropIndex("dbo.SaleProducts", new[] { "Product_Id" });
            DropIndex("dbo.SaleProducts", new[] { "Sale_Id" });
            AlterColumn("dbo.Sales", "EmailAddress", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Products", "LastModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Products", "CreatedDate", c => c.DateTime(nullable: false));
            DropTable("dbo.SaleProducts");
            CreateIndex("dbo.Products", "Sale_Id");
            AddForeignKey("dbo.Products", "Sale_Id", "dbo.Sales", "Id");
        }
    }
}
