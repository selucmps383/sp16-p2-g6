using System.Web.Helpers;
using _383_PhaseII_Group6_API.Models;

namespace _383_PhaseII_Group6_API.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_383_PhaseII_Group6_API.Models.ProjectContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_383_PhaseII_Group6_API.Models.ProjectContext context)
        {
            context.Users.AddOrUpdate(x => x.Id, new User { Id = 1, FirstName = "Admin", LastName = "User", Email = "sa@383.com", Password = Crypto.HashPassword("password"),ApiKey  = null});
            context.Products.AddOrUpdate(x => x.Id, new Product { Id = 1, Name = "Apple", CreatedDate = new DateTime(2006, 3, 21, 2, 0, 0), Price = 10, InventoryCount = 99, Category = "Fruit", Manufacturer = "Fiji", LastModifiedDate = new DateTime(2006, 3, 21, 2, 0, 0) });
            context.Products.AddOrUpdate(x => x.Id, new Product { Id = 2, Name = "Pizza", CreatedDate = new DateTime(2006, 3, 21, 2, 0, 0), Price = 900, InventoryCount = 99, Category = "Meal", Manufacturer = "Italy", LastModifiedDate = new DateTime(2006, 3, 21, 2, 0, 0) });
            context.Products.AddOrUpdate(x => x.Id, new Product { Id = 3, Name = "Lasagna", CreatedDate = new DateTime(2006, 3, 21, 2, 0, 0), Price = 8, InventoryCount = 99, Category = "Meal", Manufacturer = "Italy", LastModifiedDate = new DateTime(2006, 3, 21, 2, 0, 0) });
            context.Products.AddOrUpdate(x => x.Id, new Product { Id = 4, Name = "Banana", CreatedDate = new DateTime(2006, 3, 21, 2, 0, 0), Price = 700, InventoryCount = 99, Category = "Fruit", Manufacturer = "Honduras", LastModifiedDate = new DateTime(2006, 3, 21, 2, 0, 0) });
            
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

        }
    }
}
