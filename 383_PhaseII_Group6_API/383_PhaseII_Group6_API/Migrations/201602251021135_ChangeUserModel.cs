namespace _383_PhaseII_Group6_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeUserModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "ApiKey", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "ApiKey", c => c.String(nullable: false, maxLength: 150));
        }
    }
}
