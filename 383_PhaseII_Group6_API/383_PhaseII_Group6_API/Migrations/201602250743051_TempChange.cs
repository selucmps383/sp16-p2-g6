namespace _383_PhaseII_Group6_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TempChange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Manufacturer", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Manufacturer", c => c.String(nullable: false, maxLength: 150));
        }
    }
}
